package Test.gfx.simplegfx;

import Test.grid.GridDirection;
import Test.grid.position.AbstractGridPosition;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Position extends AbstractGridPosition {

    private Rectangle rectangle;
    private Grid grid;
    private Picture picture;

    public Position(Grid grid, String str){
        super((int) (Math.random() * grid.getCols()), (int) (Math.random() * grid.getRows()), grid);

        this.grid = grid;

        int x = this.grid.columnToX(getCol());
        int y = this.grid.rowToY(getRow());

         //this.rectangle = new Rectangle(x, y, this.grid.getCellSize(), this.grid.getCellSize());
        this.picture = new Picture(x,y,str);
        //this.picture.grow(-80,-80);
        show();
    }

    public Position(int col, int row, Grid grid){
        super(col, row, grid);

        this.grid = grid;

        int x = this.grid.columnToX(col);
        int y = this.grid.rowToY(row);

        // this.rectangle = new Rectangle(x, y, this.grid.getCellSize(), this.grid.getCellSize());
        //this.rectangle.setColor(Color.BLUE);
       // show();
    }

    @Override
    public void show() {
        //this.rectangle.fill();
        this.picture.draw();
    }

    @Override
    public void hide() {
        this.rectangle.delete();
    }

    @Override
    public void moveInDirection(GridDirection direction, int distance) {

        int initialCol = getCol();
        int initialRow = getRow();

        super.moveInDirection(direction, distance);

        int dx = grid.columnToX(getCol()) - grid.columnToX(initialCol);
        int dy = grid.rowToY(getRow()) - grid.rowToY(initialRow);

        //this.rectangle.translate(dx,dy);
        this.picture.translate(dx,dy);

    }

}
