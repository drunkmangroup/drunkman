package Test.grid;

import Test.grid.position.GridPosition;
import Test.player.PlayerFac;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public interface Grid {

    public void init();

    public int getCols();

    public int getRows();

    public GridPosition makeGridPosition();

    public GridPosition makeGridPosition(int col, int row);

    public boolean hasCollided(PlayerFac player);
}
