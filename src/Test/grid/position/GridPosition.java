package Test.grid.position;

import Test.grid.GridDirection;
import Test.grid.GridColor;

public interface GridPosition {

    public int getCol();

    public int getRow();

    public void setPos(int col, int row);
    

    public void show();

    public void hide();

    public void moveInDirection(GridDirection direction, int distance);

    public boolean equals(GridPosition position);

    public GridColor getColor();

    public void setColor(GridColor color);
}
