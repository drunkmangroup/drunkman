package Test.player;

import Test.grid.GridDirection;
import Test.grid.position.GridPosition;
import Test.walls.WallsFactory;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class PlayerPlayer2 extends PlayerFac implements KeyboardHandler {

    private final int MAX_SPEED = 3;
    private Keyboard keyboard;
    private int speed = 0;
    private int score = 1;
    private boolean u = false;
    private boolean d = false;
    private boolean l = false;
    private boolean r = false;
    public boolean check = false;



    public PlayerPlayer2(GridPosition pos) {
        super(pos);
        keyboard = new Keyboard(this);
        init();
    }


    public void init() {
        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_A);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_D);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent up = new KeyboardEvent();
        up.setKey(KeyboardEvent.KEY_W);
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent down = new KeyboardEvent();
        down.setKey(KeyboardEvent.KEY_S);
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);


        KeyboardEvent leftR = new KeyboardEvent();
        leftR.setKey(KeyboardEvent.KEY_A);
        leftR.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);

        KeyboardEvent rightR = new KeyboardEvent();
        rightR.setKey(KeyboardEvent.KEY_D);
        rightR.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);

        KeyboardEvent upR = new KeyboardEvent();
        upR.setKey(KeyboardEvent.KEY_W);
        upR.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);

        KeyboardEvent downR = new KeyboardEvent();
        downR.setKey(KeyboardEvent.KEY_S);
        downR.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);


        keyboard.addEventListener(leftR);
        keyboard.addEventListener(rightR);
        keyboard.addEventListener(upR);
        keyboard.addEventListener(downR);
        keyboard.addEventListener(left);
        keyboard.addEventListener(right);
        keyboard.addEventListener(up);
        keyboard.addEventListener(down);
    }

    @Override
    public void move() {
        accelerate(currentDirection, speed);
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    @Override
    public void accelerate(GridDirection direction, int speed) {

        this.currentDirection = direction;
        for (int i = 0; i < speed; i++) {
            getPos().moveInDirection(direction, 1);

        }


        if (this.getGrid().hasCollided(this)) {
            //Decrease player speed
        }

    }

    public void m() {
        if(l) {
            currentDirection = GridDirection.LEFT;
        }
        if(r) {
            currentDirection = GridDirection.RIGHT;
        }
        if(u) {
            currentDirection = GridDirection.UP;
        }
        if(d) {
            currentDirection = GridDirection.DOWN;
        }
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_A:

                for (int i = 0; i < WallsFactory.getWalls().length; i++) {

                    if (getPos().getCol() - 1 == WallsFactory.getWalls()[i].getPos().getCol() && getPos().getRow() == WallsFactory.getWalls()[i].getPos().getRow()) {
                        return;
                    }
                }

                l = true;

                m();
                //currentDirection = GridDirection.LEFT;
                break;

            case KeyboardEvent.KEY_D:
                for (int i = 0; i < WallsFactory.getWalls().length; i++) {

                    if (getPos().getCol() + 1 == WallsFactory.getWalls()[i].getPos().getCol() && getPos().getRow() == WallsFactory.getWalls()[i].getPos().getRow()) {

                        return;
                    }
                }
                r = true;
                m();
                //  currentDirection = GridDirection.RIGHT;
                break;
            case KeyboardEvent.KEY_W:
                for (int i = 0; i < WallsFactory.getWalls().length; i++) {

                    if (getPos().getCol() == WallsFactory.getWalls()[i].getPos().getCol() && getPos().getRow() - 1 == WallsFactory.getWalls()[i].getPos().getRow()) {

                        return;
                    }
                }
                u = true;
                m();
                //currentDirection = GridDirection.UP;
                break;
            case KeyboardEvent.KEY_S:
                for (int i = 0; i < WallsFactory.getWalls().length; i++) {

                    if (getPos().getCol() == WallsFactory.getWalls()[i].getPos().getCol() && getPos().getRow() + 1 == WallsFactory.getWalls()[i].getPos().getRow()) {

                        return;
                    }
                }
                d = true;
                m();
                //currentDirection = GridDirection.DOWN;
                break;
        }


        if (speed == 0) {
            accelerate(currentDirection, 1);
        }

    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_A:
                l = false;
                break;
            case KeyboardEvent.KEY_D:
                r = false;
                break;
            case KeyboardEvent.KEY_W:
                u = false;
                break;
            case KeyboardEvent.KEY_S:
                d = false;
                break;

        }
    }

    public int getSpeed() {
        return speed;
    }
}
