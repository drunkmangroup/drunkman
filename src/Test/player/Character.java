package Test.player;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public enum Character {
    BENNY("Benny"),
    PAULINHO("Paulinho"),
    ROLO("Rolo-Demon");

    private String name;

    Character(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
