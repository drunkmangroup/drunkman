package Test.player;

import Test.grid.Grid;
import Test.grid.GridDirection;
import Test.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.awt.*;

abstract public class PlayerFac implements KeyboardHandler {

    private GridPosition pos;
    private Grid grid;
    private int speed;
    private boolean alive;
    protected Keyboard keyboard;
    protected Rectangle rectangle;
    protected Character characters;
    private int score;
    protected Character character;
    private boolean picked;
    private boolean check = false;

    protected GridDirection currentDirection;

    public PlayerFac(GridPosition pos) {

        this.pos = pos;
        score = 0;
        alive = true;
        keyboard = new Keyboard(this);
        characters = null;
        rectangle = null;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    public Grid getGrid() {
        return this.grid;
    }

    public GridPosition getPos() {
        return pos;
    }

    public abstract void move();



    public void accelerate(GridDirection direction, int speed) {


        GridDirection newDirection = direction;


        if (isHittingWall()) {
            newDirection = direction.oppositeDirection();
        }


        this.currentDirection = newDirection;
        for (int i = 0; i < speed; i++) {
            getPos().moveInDirection(newDirection, 1);

        }

    }

    public boolean isHittingWall() {

        switch (currentDirection) {
            case LEFT:
                if (getPos().getCol() == 0) {
                    return true;
                }
                break;
            case RIGHT:
                if (getPos().getCol() == grid.getCols() - 1) {
                    return true;
                }
                break;
            case UP:
                if (getPos().getRow() == 0) {
                    return true;
                }
                break;
            case DOWN:
                if (getPos().getRow() == grid.getRows() - 1) {
                    return true;
                }
        }

        return false;

    }
    public void setCharacter(Character character) {
        this.character = character;
    }public int getScore () {
        return score;
    }

    public void setScore ( int score){
        this.score = score;
    }

    public void setPos (GridPosition pos){
        this.pos = pos;
    }
    public void isPicked() {
        picked = true;
    }
    public boolean getPicked() {
        return picked;


    }
}