package Test.gameObject;

import Test.grid.Grid;
import Test.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class GameObject {

    private GridPosition pos;
    private Grid grid;
    protected Picture picture;
    private boolean sameSpace;

    public GameObject() {
        sameSpace = false;
    }

    public void setSameSpace(boolean sameSpace) {

        this.sameSpace = true;
        picture.delete();
    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }


    public GridPosition getPos() {
        return pos;

    }

    public void setPos(GridPosition pos) {
        this.pos = pos;
    }

    public Picture getPicture() {
        return picture;
    }
}
