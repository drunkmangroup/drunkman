package Test.gameObject;

import Test.grid.Grid;
import Test.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Beer extends GameObject {


    private GridPosition pos;
    private Grid grid;
    private Picture picture;


    public void initPicture() {

        int newX = getPos().getCol();
        int newY = getPos().getRow();
        this.picture = new Picture(newX, newY, "Beer.png");
        this.getPicture().draw();
    }

    @Override
    public Picture getPicture() {
        return super.getPicture();
    }

    @Override
    public Grid getGrid() {
        return grid;
    }

    @Override
    public GridPosition getPos() {
        return pos;
    }

    @Override
    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    @Override
    public void setPos(GridPosition pos) {
        this.pos = pos;
    }

    public void setSameSpace() {
        super.setSameSpace(true);
    }
}
