package Test.gameObject;

import Test.gfx.simplegfx.Grid;
import Test.gfx.simplegfx.Position;

public class GameObjectFactory {

    Grid grid;

    public static GameObject[] gameObjects;
    private static int number = 2;

    public static GameObject getNewObjects(GameObjectType type, Grid grid) {

        GameObject beer = new Beer();
        GameObject pizza= new Pizza();
        //Position position = new Position(grid);

        //beer.setPos(position);
        //pizza.setPos(position);

        if (GameObjectType.BEER == type) {
            return beer;
        }else {
            return pizza;
        }
    }
}
